import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CharacterClass } from '../classes/character-class';
import { ResponseMessage } from '../classes/response-message';
import * as Constants from 'src/app/classes/constants';


@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) {

  }

  getCharacters() : Promise<CharacterClass[]> {
    return this.http.get<CharacterClass[]>(Constants.ControllerBackendURL + '/list',  { withCredentials: true }).toPromise();
  }

  getCharacter(id: number) : Promise<CharacterClass> {
    let params = new HttpParams().set('id', id);
    return this.http.get<CharacterClass>(Constants.ControllerBackendURL + '/get',  { params: params, withCredentials: true }).toPromise();
  }

  addCharacter(data: CharacterClass) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type':  'application/json'}),
      withCredentials: true
    };
    return this.http.put<ResponseMessage>(Constants.ControllerBackendURL + '/add', data, httpOptions).toPromise();
  }

  updateCharacter(data: CharacterClass) {
    var httpOptions = {
      headers: new HttpHeaders({'Content-Type':  'application/json'}),
      withCredentials: true
    };
    return this.http.put<ResponseMessage>(Constants.ControllerBackendURL + '/update', data, httpOptions).toPromise();
  }

  deleteCharacter(id: number) {
    let params = new HttpParams().set('id', id);
    return this.http.post<ResponseMessage>(Constants.ControllerBackendURL + '/delete',  { params: params, withCredentials: true }).toPromise();
  }

  searchCharacters(searchText: string) : Promise<CharacterClass[]> {
    let params = new HttpParams().set('name', searchText);
    return this.http.get<CharacterClass[]>(Constants.ControllerBackendURL + '/search',  { params: params, withCredentials: true }).toPromise();
  }
}
