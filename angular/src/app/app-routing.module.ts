import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterListComponent } from './components/character-list/character-list.component';
import * as Constants from './classes/constants';
import { CharacterProfileComponent } from './components/character-profile/character-profile.component';
import { CharacterBuilderComponent } from './components/character-builder/character-builder.component';



const routes: Routes = [
  { path: Constants.PATH_MainURL, component: CharacterListComponent},
  { path: Constants.PATH_CharacterInfoURL + '/:id', component: CharacterProfileComponent},
  { path: Constants.PATH_AddCharacterURL, component: CharacterBuilderComponent },
  { path: '', redirectTo: Constants.PATH_MainURL, pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
