import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CharacterClass } from 'src/app/classes/character-class';
import { BackendService } from 'src/app/services/backend.service';
import * as Constants from 'src/app/classes/constants';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-character-profile',
  templateUrl: './character-profile.component.html',
  styleUrls: ['./character-profile.component.css']
})
export class CharacterProfileComponent implements OnInit {

  character_id!: number;

  @ViewChild('NameInput') nameInput!: ElementRef;
  @ViewChild('DescriptionInput') descriptionInput!: ElementRef;

  isWorking: boolean = false;

  constructor(private router: Router, private backendService: BackendService, private activatedRoute: ActivatedRoute) 
  { 
    var id = this.router.url.split('?')[0].split('/').pop()
    if (id != undefined) this.character_id = parseInt(id);
  }

  updateUI() {
    
    this.isWorking = true;
    this.backendService.getCharacter(this.character_id).then(
      result => {
        this.isWorking = false;
        this.nameInput.nativeElement.value = result.name;
        this.descriptionInput.nativeElement.value = result.description;
      },
      error => {
        this.isWorking = false;
        alert(error);
      },
    );
  }

  ngOnInit(): void {
    this.updateUI();
  }

  updateCharacter() : void {

    if (this.character_id == null || this.character_id == undefined) return;

    var name = this.nameInput.nativeElement.value;
    var description = this.descriptionInput.nativeElement.value;

    var newClass = new CharacterClass();
    newClass.description = description;
    newClass.name = name;
    newClass.id = this.character_id;
  
    this.isWorking = true;
    this.backendService.updateCharacter(newClass).then(
      result => {
        this.isWorking = false;
        this.router.navigateByUrl(Constants.PATH_MainURL);
      },
      error => {
        this.isWorking = false;
        alert(error);
      },
    );

  }

  goBack() : void {
    this.router.navigateByUrl(Constants.PATH_MainURL);
  }

}
