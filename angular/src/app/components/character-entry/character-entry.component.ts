import { Component, Directive, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CharacterClass } from 'src/app/classes/character-class';
import { BackendService } from 'src/app/services/backend.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as Constants from 'src/app/classes/constants';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-character-entry',
  templateUrl: './character-entry.component.html',
  styleUrls: ['./character-entry.component.css']
})

export class CharacterEntryComponent implements OnInit
{
  get postData(): CharacterClass {  return this._postData; }
  @Input() set postData(value:CharacterClass) {  this._postData = value; this.updateUI(this._postData); }
  private _postData!: CharacterClass;

  
  comments: Comment[] = [];
  name: string = 'First';
  description: string = 'Description Text';
  
  constructor(private backendService: BackendService, private router: Router, private route: ActivatedRoute, public dialog: MatDialog) {}

  goBackwards() {

  }

  goForwards() {
  }


  updateLikeIcon(data: CharacterClass) {

  }

  updateLikeCount(data: CharacterClass)  {

  }

  updateComments(data: CharacterClass) {

  }

  updateCommentCount(data: CharacterClass) {

  }

  updateUI(data: CharacterClass)  {
    if (data == null) return;
    
    this.name = data.name;
    this.description = data.description;
  }

  ngOnInit(): void {

  }

  goToProfile() {
    this.router.navigateByUrl(Constants.PATH_CharacterInfoURL + '/' + this.postData.id, { });
  }  

  delete() {
    this.backendService.deleteCharacter(this.postData.id).then(() => {
      this.router.navigateByUrl(Constants.PATH_MainURL, { });
    });
  }

  createComment() : void {

  }

  likePost() : void {

  }

  updateButtons() {

  }

}
