import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router'
import { CharacterClass } from 'src/app/classes/character-class';
import { BackendService } from 'src/app/services/backend.service';
import { Router } from "@angular/router"
import * as Constants from 'src/app/classes/constants';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from "rxjs/operators"
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit, OnDestroy {

  showSearchBar: boolean = false;
  postPromise!: Promise<CharacterClass[]>;
  isPostsWaiting: boolean = false;

  @ViewChild('SearchBox') searchBox!: ElementRef;

  posts!: CharacterClass[];

  constructor(private route: ActivatedRoute, private backendService: BackendService, private router: Router, public dialog: MatDialog) 
  { 

  }

  createPost() : void 
  {
    this.router.navigateByUrl(Constants.PATH_AddCharacterURL);
  }

  updateUI() {
    this.posts = [];
    this.isPostsWaiting = true;
    this.postPromise = this.backendService.getCharacters();
    this.updatePosts();
  }

  updatePosts() {
    this.postPromise.then(result => {
      this.posts = result;
      this.isPostsWaiting = false;
    },
    error => {
      this.isPostsWaiting = false;
    });
  }

  ngOnInit(): void {
    this.updateUI();
  }

  ngOnDestroy(): void {
    
  }

  goHome() {
    this.router.navigateByUrl(Constants.PATH_MainURL);
  }

  search() {
    var text = this.searchBox.nativeElement.value;
    this.posts = [];
    this.isPostsWaiting = true;
    this.postPromise = this.backendService.searchCharacters(text);
    this.updatePosts();
  }

  toggleSearch() {
    if (this.showSearchBar) this.updateUI();
    this.showSearchBar =! this.showSearchBar 
  }

}
