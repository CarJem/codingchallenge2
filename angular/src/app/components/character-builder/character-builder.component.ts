import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CharacterClass } from 'src/app/classes/character-class';
import * as Constants from 'src/app/classes/constants';
import { BackendService } from 'src/app/services/backend.service';

@Component({
  selector: 'app-character-builder',
  templateUrl: './character-builder.component.html',
  styleUrls: ['./character-builder.component.css']
})
export class CharacterBuilderComponent implements OnInit {

  

  @ViewChild('NameInput') nameInput!: ElementRef;
  @ViewChild('DescriptionInput') descriptionInput!: ElementRef;

  isWorking: boolean = false;

  constructor(private router: Router, private backendService: BackendService) { }

  ngOnInit(): void {
  }

  createAccount() : void {
    var name = this.nameInput.nativeElement.value;
    var description = this.descriptionInput.nativeElement.value;

    var newClass = new CharacterClass();
    newClass.description = description;
    newClass.name = name;
    
    this.isWorking = true;
    this.backendService.addCharacter(newClass).then(
      result => {
        this.isWorking = false;
        this.router.navigateByUrl(Constants.PATH_MainURL);
      },
      error => {
        this.isWorking = false;
        alert(error);
      },
    );

  }

  goBack() : void {
    this.router.navigateByUrl(Constants.PATH_MainURL);
  }

}
