package cc2.controller;

import java.net.http.HttpResponse;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cc2.dao.CharacterDAOService;
import cc2.model.Character;

@RestController
@CrossOrigin(origins={"http://localhost:4200", "http://34.125.43.171:4200", "http://localhost:4040"}, allowCredentials = "true")
@RequestMapping("/characters")
public class CharacterController {
	
	final static Logger logger = Logger.getLogger(CharacterController.class);
	private CharacterDAOService characterService;
	
	public CharacterController() {
	}
	
	public CharacterController(CharacterDAOService daoService) {
		super();
		this.characterService = daoService;
	}
	
	public CharacterDAOService getDaoService() {
		return characterService;
	}
	
	@Autowired
	public void setDaoService(CharacterDAOService daoService) {
		this.characterService = daoService;
	}
	
	@PutMapping(value="/update")
	public void updateCharacter(@RequestBody Character newCharacter) {
		Character toSave = this.characterService.findById(newCharacter.getId());
		toSave.setName(newCharacter.getName());
		toSave.setDescription(newCharacter.getDescription());
		this.characterService.save(toSave);
	}
	
	@PostMapping(value="/delete")
	public void deleteCharacter(@RequestParam int id) {
		this.characterService.deleteById(id);
	}
	
	@GetMapping(value="/search")
	public List<Character> deleteCharacter(@RequestParam String name) {
		return this.characterService.search(name);
	}
	
	@PutMapping(value="/add")
	public void addCharacter(@RequestBody Character newCharacter) {
		this.characterService.save(newCharacter);
	}
	
	@GetMapping(value="/get")
	public Optional<Character> getAllCharacters(@RequestParam Integer id) {
		return this.characterService.findById(id);
	}
	
	@GetMapping(value="/list")
	public List<Character> getAllCharacters() {
		return this.characterService.findAll();
	}
}
