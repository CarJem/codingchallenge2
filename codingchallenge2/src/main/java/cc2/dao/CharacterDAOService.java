package cc2.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cc2.model.*;
import cc2.model.Character;

public interface CharacterDAOService extends JpaRepository<Character, Integer> 
{
	public Character findById(int id);

	@Query(value = "SELECT * FROM CHARACTER WHERE CHARACTER_NAME LIKE %?1", nativeQuery = true)
	public List<Character> search(String name);
}
